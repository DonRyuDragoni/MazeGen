# MazeGen

A simple maze generator program built in Lua using the LÖVE engine.

This work was inspired by [Daniel Shiffman's Coding Challenges][] on the same
topic.

[Daniel Shiffman's Coding Challenges]: https://www.youtube.com/watch?v=HyK_Q5rrcr4

## Alternative implementations

You can find them in the different branches of this project, but, for
convenience, and since most don't have a README of their own, here are the
links and a small description of each:

- The [C version][] uses SDL2 and includes a (very simple) Makefile to build
  (sorry, Windows users, you'll have to compile it on your own).

[C version]: https://gitlab.com/DonRyuDragoni/MazeGen/tree/c-sdl2

- The [Zig version][] is a rewrite of the above, also using SDL2.

[Zig version]: https://gitlab.com/DonRyuDragoni/MazeGen/tree/zig-sdl2

- The [Rust version][] uses only [bindgen][] (to generate [SDL2][] bindings)
  and [rand][] as external dependencies.

[rust version]: https://gitlab.com/DonRyuDragoni/MazeGen/tree/rust-sdl2
[bindgen]: https://gitlab.com/rust-lang-nursery/rust-bindgen
[rand]: https://gitlab.com/rust-lang-nursery/rand
[sdl2]: https://www.libsdl.org/

- The [Haskell version][] uses [Gloss][] and [random][].

[haskell version]: https://gitlab.com/DonRyuDragoni/MazeGen/tree/haskell-gloss
[gloss]: https://hackage.haskell.org/package/gloss
[random]: https://hackage.haskell.org/package/random-1.1

- And there's the [Godot version][].

  I made this mainly to learn the engine and play with GDNative, so expect a
  messy and useless repo.

  Note: This version may contain build artifacts and unnecessary files. I'll
  (hopefully) clean the repository someday.

[godot version]: https://gitlab.com/DonRyuDragoni/MazeGen/tree/godot
