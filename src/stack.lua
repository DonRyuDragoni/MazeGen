local mod = {}

mod.Stack = {}
mod.Stack.__index = mod.Stack

mod.newStack = function(firstEl)
   local newStack = {firstEl}

   return setmetatable(newStack, mod.Stack)
end

mod.Stack.push = function(self, el)
   table.insert(self, el)
end

mod.Stack.pop = function(self)
   local last = self[#self]

   -- Remove the element from the sack and return it
   self[#self] = nil
   return last
end

return mod
