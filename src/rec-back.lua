--[[
This module implements the Depth-first search recursive backtracker algorithm
for maze generation.

The algorithm works as follows:

--]]

local mod = {}

local stack = require "./stack"

mod.Generator = {}
mod.Generator.__index = mod.Generator

mod.newGenerator = function(maze, startPos)
   local startCell = maze.cells[startPos.i][startPos.j]

   local newGenerator = {
      maze      = maze,
      startCell = startCell,
      headCell  = startCell,

      stack = stack.newStack(startCell)
   }

   return setmetatable(newGenerator, mod.Generator)
end

mod.Generator.unvisitedNeighbours = function(self)
   local n = {}

   local i = self.headCell.i
   local j = self.headCell.j

   local rows  = self.maze.rows
   local cols  = self.maze.cols
   local cells = self.maze.cells

   local top    = j > 1    and cells[i][j-1] or nil
   local right  = i < cols and cells[i+1][j] or nil
   local bottom = j < rows and cells[i][j+1] or nil
   local left   = i > 1    and cells[i-1][j] or nil

   if top    and not top.visited    then table.insert(n, top   ) end
   if right  and not right.visited  then table.insert(n, right ) end
   if bottom and not bottom.visited then table.insert(n, bottom) end
   if left   and not left.visited   then table.insert(n, left  ) end

   return n
end

mod.Generator.step = function(self)
   local neighbours = self:unvisitedNeighbours()

   if #neighbours > 0 then
      local chosen = neighbours[math.random(#neighbours)]

      self.stack:push(chosen)
      self.maze:removeWalls(self.headCell, chosen)

      self.headCell.visited = true
      self.headCell = chosen
   elseif #self.stack > 0 then
      self.headCell.visited = true
      self.headCell = self.stack:pop()
   end

   return self.headCell
end

return mod
