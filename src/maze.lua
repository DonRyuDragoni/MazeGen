local mod = {}

local cell = require "./cell"

mod.Maze = {}
mod.Maze.__index = mod.Maze

mod.newMaze = function(rows, cols, cellsize)
   -- Create the grid of cells
   local cells = {}
   for i = 1, rows do
      -- create each row of cells as an empty table
      cells[i] = {}
      for j = 1, cols do
         cells[i][j] = cell.newCell(i, j, cellsize)
      end
   end

   -- Construct the new object
   local newMaze = {
      rows = rows,
      cols = cols,

      cells = cells
   }

   return setmetatable(newMaze, mod.Maze)
end

mod.Maze.draw = function(self, currentHead)
   for i = 1, self.rows do
      for j = 1, self.cols do
         self.cells[i][j]:draw(i == currentHead.i and j == currentHead.j)
      end
   end
end

mod.Maze.removeWalls = function(self, a, b)
   local iDiff = a.i - b.i
   local jDiff = a.j - b.j

   if iDiff > 0 then
      a.walls.left  = false
      b.walls.right = false
   elseif iDiff < 0 then
      a.walls.right = false
      b.walls.left  = false
   elseif jDiff > 0 then
      a.walls.top    = false
      b.walls.bottom = false
   elseif jDiff < 0 then
      a.walls.bottom = false
      b.walls.top    = false
   end
end

return mod
