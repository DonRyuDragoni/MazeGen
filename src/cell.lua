local mod = {}

mod.Cell = {}
mod.Cell.__index = mod.Cell

mod.newCell = function(i, j, size)
   local newCell = {
      i = i,
      j = j,

      size = size,

      visited = false,

      walls = {
         top    = true,
         right  = true,
         bottom = true,
         left   = true
      }
   }

   return setmetatable(newCell, mod.Cell)
end

mod.Cell.draw = function(self, head)
   local x = (self.i - 1)*self.size
   local y = (self.j - 1)*self.size

   local nextX = (self.i)*self.size
   local nextY = (self.j)*self.size

   local r, g, b, a = love.graphics.getColor()

   if head then
      love.graphics.setColor(0, 0, 255, 100)
      love.graphics.rectangle("fill", x, y, self.size, self.size)
   elseif self.visited then
      love.graphics.setColor(0, 255, 0, 100)
      love.graphics.rectangle("fill", x, y, self.size, self.size)
   end

   love.graphics.setColor(r, g, b, a)

   if self.walls.top then
      love.graphics.line(x, y, nextX, y)
   end

   if self.walls.right then
      love.graphics.line(nextX, y, nextX, nextY)
   end

   if self.walls.bottom then
      love.graphics.line(nextX, nextY, x, nextY)
   end

   if self.walls.left then
      love.graphics.line(x, nextY, x, y)
   end
end

return mod
