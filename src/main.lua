local maze = require "./maze"
local gen  = require "./rec-back"

local width  = 400
local height = 400

local seed, paused, grid, current

love.load = function()
   seed = 0
   --seed = os.time()
   math.randomseed(seed)

   love.window.setTitle("Maze Generator")
   love.window.setMode(width, height)

   grid      = maze.newMaze(10, 10, width/10)
   generator = gen.newGenerator(grid, {i = 1, j = 1})
end

love.keypressed = function(key, _, _)
   if key == "escape" then
      love.event.quit()
   elseif key == "p" then
      paused = not paused
   elseif key == "r" then
      math.randomseed(seed)

      grid      = maze.newMaze(10, 10, width/10)
      generator = gen.newGenerator(grid, {i = 1, j = 1})
   end
end

love.update = function(_)
   if not paused then
      current = generator:step()
   end
end

love.draw = function()
   grid:draw(current)
end
